﻿using System;
using static System.Console;
using System.Linq;
using Communicator;
using System.Net;
using Communicator.Common;
using System.IO;
using System.Threading;

namespace Client
{


    class Client
    {
        static void Main(string[] args)
        {
            //Write("Port to listen on:");
            var port = 3000; //Convert.ToInt32(ReadLine());

            var dataBuffer = new DataBuffer<string>(1000000, false, true);
            var policy = new CommunicationPolicy(TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(60), 3, 1000000);
            var sink = new DataSink(policy, dataBuffer, new IPEndPoint(IPAddress.Parse("127.0.0.1"), port));


            var t1 = new Thread(x => {
                var buffer = x as IObjectBuffer;

                using (var wr = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\data1.txt"))
                {
                    while (true)
                    {
                        if (!buffer.IsEmpty)
                        {
                            var objs = buffer.Read(policy.MaxObjectsToSend);
                            wr.WriteLine(string.Join(" | ", objs));
                            wr.Flush();
                        }
                    }
                }
            });

            var t2 = new Thread(x => {
                var buffer = x as IObjectBuffer;

                using (var wr = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\data2.txt"))
                {
                    while (true)
                    {
                        if (!buffer.IsEmpty)
                        {
                            var objs = buffer.Read(policy.MaxObjectsToSend);
                            wr.WriteLine(string.Join(" | ", objs));
                            wr.Flush();
                        }
                    }
                }
            });

            t1.Start(dataBuffer);
            t2.Start(dataBuffer);

            while (!(Console.KeyAvailable? Console.ReadKey().Key == ConsoleKey.Escape: false))
                sink.Run();

            t1.Abort();
            t2.Abort();


        }




    }
}
