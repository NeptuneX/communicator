﻿using System;

namespace Communicator
{
    public sealed class CommunicationPolicy
    {
        public TimeSpan MessageTimeout { get; }
        public TimeSpan MaxIdleTime { get; }
        public int MaxRetries { get; }
        public int MaxObjectsToSend { get; }

        public CommunicationPolicy(TimeSpan messageTimeout, TimeSpan idletime, int maxretries, int maxobjects)
        {
            MessageTimeout = messageTimeout;
            MaxIdleTime = idletime;
            MaxRetries = maxretries;
            MaxObjectsToSend = maxobjects;
        }


        public bool IsTimedOut(DateTime last)
        {
            var ret = false;

            if(last != DateTime.MinValue)
            {
                ret = DateTime.UtcNow.Subtract(last).TotalMilliseconds >= this.MessageTimeout.TotalMilliseconds;
            }
            return ret;
        }
        public bool IsRetryAllowed(int attempts)
        {
            return attempts < MaxRetries;
        }
        public bool IsIdle(DateTime lastTime)
        {
            return DateTime.UtcNow.Subtract(lastTime).TotalMilliseconds >= MaxIdleTime.TotalMilliseconds;
        }
    }
}
