﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Text;
using Communicator.Common;
using NetworkChannel;
using NetworkChannel.Common;

namespace Communicator
{
    public class DataSink: IDisposable
    {
       readonly INetworkChannel _channel;
       readonly IDictionary<string, ConnectionStatus> _sources;

       readonly IObjectBuffer _buffer;
       readonly CommunicationPolicy _policy;

      
        public DataSink(CommunicationPolicy policy, IObjectBuffer buffer, IPEndPoint host)
        {
            _channel = new ChannelReader<DefaultObjectSerializer>(host);
            _sources = new Dictionary<string, ConnectionStatus>();
            _buffer = buffer;
            _policy = policy;
        }


           public void Run() {

            var signals = _channel.ReceiveSignal(_policy.MessageTimeout);
            var filteredSignals = signals.Where(x => (x.Length > 0) && !_policy.IsTimedOut((DateTime) x.Arguments[0]));
            if (filteredSignals.Any())
                ProcessSignals(signals);

            if (_sources.Any(x => x.Value.State == (int) SourceState.Hot))
            {
                var hotSources = _sources.Where(x => x.Value.State == (int) SourceState.Hot);
                Contract.Assert(hotSources.Count() == 1, "more than 1 hot sources");

                var hot = hotSources.FirstOrDefault().Value;
                var hotSender = hotSources.FirstOrDefault().Key;

                var shouldReset = false;
                var data = _channel.ReceiveData(_policy.MessageTimeout);


                if (data.Any())  // received the data so enable other sources 
                {
                    shouldReset = true;

                    foreach (var d in data)
                    {
                        if (d.Sender == hotSender)
                        {
                            var written = 0;
                            if (_buffer.Write(d.Data, out written))
                                _channel.SendSignal(d.Sender, (int) GridSignal.RECEIVED, DateTime.Now, d.Key);
                            else
                                _channel.SendSignal(d.Sender, (int) GridSignal.RETRANSMIT, DateTime.Now, d.Key);

                            PRINT($"Received data from {d.Sender}");
                        }
                        else
                        {
                            PRINT($"Unexpected data received, discarding {d.Sender}");
                        }
                    }

                    if (_sources.ContainsKey(hotSender))
                    {
                        var source = _sources[hotSender];
                        source.RegisterDataActivity();
                        source.Change((int) SourceState.Connected);
                    }
                }
                else
                {
                    //Determine if timed out?
                    if (_policy.IsTimedOut(hot.LastReceivedAt))
                    {
                        shouldReset = true;   //tell the source to retry if it has anything to send, enable other sources.
                        hot.Change((int) SourceState.Connected);
                        _channel.SendSignal(hotSender, (int) GridSignal.CONTINUE, DateTime.Now);

                        PRINT($"Hot source timed out {hotSender}");
                    }
                }
                
                if (shouldReset)  //should enable others?
                {
                    foreach (var it in _sources.Where(x => x.Key != hotSender))
                    {
                        it.Value.Change((int) SourceState.Connected);
                        _channel.SendSignal(it.Key, (int) GridSignal.CONTINUE, DateTime.UtcNow);
                    }
                }
            }
        }


        private void ProcessSignals(IEnumerable<CommandMessage> signals)
        {
            foreach (var signal in signals)
                switch ((GridSignal) signal.Command)
                {
                    case GridSignal.HELLO:  // a new source is trying to connect.
                        if (!_sources.ContainsKey(signal.Sender))
                        {
                            var status = new ConnectionStatus((int) SourceState.Connected);
                            _sources.Add(signal.Sender, status);
                        }
                        _channel.SendSignal(signal.Sender, (int) GridSignal.HELLO, DateTime.Now, _buffer.Capacity);

                        PRINT($"Connected to {signal.Sender}");
                        break;

                    case GridSignal.READY2RECEIVE:   // Send this a greeen signal & tell others to wait, otherwise decline.
                        if (!_buffer.IsFull && _sources.ContainsKey(signal.Sender) && !_sources.Any(x => x.Value.State == (int) SourceState.Hot))  // is there anyone already sending anything and our buffer has space?
                        {
                            //put all others in wait state
                            var hot = signal.Sender;
                            _sources[hot].Change((int) SourceState.Hot);
                            _channel.SendSignal(hot, (int) GridSignal.READY, DateTime.Now, Math.Min(_policy.MaxObjectsToSend, _buffer.Vacancy));

                            //set all others to wait
                            var list = _sources.Where(x => x.Key != hot);
                            foreach (var it in list)
                            {
                                _channel.SendSignal(it.Key, (int) GridSignal.WAIT, DateTime.UtcNow);
                                it.Value.Change((int) SourceState.Wait);
                            }

                            PRINT($"Hot -> {signal.Sender}");
                        }
                        else
                        {
                            _channel.SendSignal(signal.Sender, (int) GridSignal.WAIT, DateTime.UtcNow);

                            PRINT($"Reques put on hold {signal.Sender}");
                        }
                        break;

                    case GridSignal.NOTHING2SEND:
                        _sources[signal.Sender].Change((int) SourceState.Connected);
                        ResetOtherSources(signal.Sender);
                        PRINT($"Hot cancelled by {signal.Sender}");
                        break;

                    case GridSignal.AREUALIVE:
                        _channel.SendSignal(signal.Sender, (int) GridSignal.IMALIVE, DateTime.UtcNow);

                        PRINT($"Pinged by {signal.Sender}");
                        break;

                    case GridSignal.GOODBYE:
                        _sources.Remove(signal.Sender);

                        PRINT($"Stopped {signal.Sender}");
                        break;
                }
        }
        void PRINT(string text)
        {
            Console.WriteLine($"{DateTime.Now.ToString("mm:ss")} {text}");
        }
        private void ResetOtherSources(string hotSource)
        {
            foreach (var it in _sources.Where(x => x.Key != hotSource))
            {
                _channel.SendSignal(it.Key, (int) GridSignal.CONTINUE, DateTime.UtcNow);
                it.Value.Change((int) SourceState.Connected);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach(var source in _sources)
                    {
                        
                    }
                    _channel.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }



        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
