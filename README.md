Communicator
============

———————————-

**Status: Closed**
*Communicator is the communicator mechanism for Grid Processing project but was developed and tested separately. As all the code has now been imported into the Grid processing, for which it was developed, this repository is closed for any modification and is no longer maintained. Please visit the Grid Processing repository for the latest code*

----------------------------

What is a Communicator?
-----------------------
Communicator is a library to communicate over internet using TCP/IP. It implements a transnational protocol for data flow and error control over sockets using NetMQ (a port of ZeroMQ). It is a part of [Grid Processing Repository] (also on Bitbucket). 

How Does it Work? 
------------------

Each communicator is able to communicate with another communicator over internet. All it needs to know is the IP address and the port. A single communicator can connect with 1000s of other communicators at the same time. Once connected they can send and receive data error free. 

Each communicator has its own cache (a disk based storage using LiteDB). When data is given to communicator, it checks which communicators are connected and "alive". and starts sending the data (even 100s of MBs) to these ones. It can also receive data from other communicators also. A single communicator is able to receive data from 100s of communicators which at the same time sending data to 100s of other communicators. 

 It implements its own protocol. An application does not have to worry about the data type, format, size or an active connection. It just sends data to communicator to send. 
 The communicator checks if there are any other communicators connected. If so, it starts sending data to one or all of them (Publish). If no receiver is connected, then it simply stores the data in its cache and waits for the receivers to connect. Once they connect, it establishes the channel and starts sending the data to them. 

The data that is cached is persistent. That means the data stays in the cache of communicator (if it is not sent out) for days and years until the receiver connects.  Thus it frees the application to worry about if the data is received or not or other issues related to such things 

If the communicator receives the data from other communicators, it informs the app about the availability of data. If the application is not listening or something then it simply stores the data in cache and lets the application know when it is listening. 

A single communicator can be a receiver, a sender or both. 

What can I achieve
------------------

With this component, you can build distributed applications that can just communicate over Internet without worry about the connectivity, the other component and stuff. It only sends the data and receives it. It doe snot have to worry about 

 - Are there any listeners?
 - Data is sent correctly?
 - Re-sending of data if lost
 - Is someone trying to send data?
 - Did we receive correct data (i.e. it is not lost or incomplete)
 - Is someone trying to send data while we are busy sending it
 - What about the data if the receivers are not connected now but will connect later? (may be hours, days or months)
 - what if someone tried sending us the data when we were offline? (hours/days/months)
 
Communicator is a part of Grid Processing. it was developed separately due to its complexity and better testing. Once stabilized, it is now a part of grid processing framework and will not be maintained separately. 

You can find more information about grid processing framework here:
  [Grid Processing Repository]: https://bitbucket.org/NeptuneX/grid-processing