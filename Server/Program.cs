﻿using Communicator;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Linq;
using Communicator.Common;

namespace Server
{
    class Server
    {
        static void Main(string[] args)
        {
            var dataBuffer = new DataBuffer<string>(20000000, false, false);

            Console.Write("Enter server port:");
            var serverPort = int.Parse(Console.ReadLine());
            var host = new IPEndPoint(IPAddress.Parse("127.0.0.1"), serverPort);


            Console.Write("Enter Echo text:");
            var echo = Console.ReadLine();

            var thread = new Thread((x) => Echo(x as IObjectBuffer, echo));
            thread.Name = "FileReader";

            var policy = new CommunicationPolicy(TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(60), 3, 100000);
            var target1 = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3000);
            
            using (var s1 = new DataSource(policy, dataBuffer, host, target1))
            {
                thread.Start(dataBuffer);
                while (thread.IsAlive || !dataBuffer.IsEmpty)
                    s1.Run();
            }
        }

        private static void ReadFile(IObjectBuffer buffer, string filePath)
        {
            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                var wait = false;
                string[] lines = null;

                while (!reader.EndOfStream)
                {
                    if (wait)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(20));
                        wait = buffer.IsFull;
                    }
                    else
                    {
                        if (lines == null)
                        {
                            var line = reader.ReadLine();
                            lines = line.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        }

                        int written = 0;
                        if (buffer.Write(lines, out written))
                        {
                            lines = null;
                        }
                        else
                        {
                            lines = lines.Skip(written).ToArray<string>();
                            wait = true;
                        }
                    }
                }
                reader.Close();
            }

        }

        private static void Echo(IObjectBuffer buffer, string text)
        {
            var wait = false;
            while (true)
            {
                if (wait)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(20));
                    wait = buffer.IsFull;
                }
                else
                {
                    int written = 0;
                    var lines = Enumerable.Repeat<string>(text, 10).ToArray<string>();
                    if (buffer.Write(lines, out written))
                    {
                            
                    }
                    else
                    {
                        wait = true;
                    }
                }
            }
            
            

        }
    }
}
